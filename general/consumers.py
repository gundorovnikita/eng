from channels.db import database_sync_to_async
import asyncio
import json
import requests
from channels.db import database_sync_to_async
from channels.consumer import AsyncConsumer
from django.contrib.auth.models import User
from general.models import Card, Profile, Task
url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?'
url_j = 'https://jisho.org/api/v1/search/words?keyword="'
key = 'trnsl.1.1.20190221T162634Z.a43e789b72aeca90.fc65f57077cb3721380ba3e01d0366d2ffcfc0ee'

class AddConsumer(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
				'type':'websocket.accept',
			})
		print('connect: ', event)

	async def websocket_receive(self,event):
		username = self.scope["user"]
		profile = Profile.objects.get(user=username)
		print('recieved: ', event)
		new_message = json.loads(event.get('text'))['mt']
		check = json.loads(event.get('text'))['q']
		print(new_message)
		print(check)
		lang = profile.pack.lang.lower()
		text = new_message.lower()
		print(lang)

		if lang == 'en-ja':
			variable_request = requests.get(url_j+text+'"').text
			diction = json.loads(variable_request)
			try:
				word = diction['data'][0]['japanese'][0]['word']
				reading = diction['data'][0]['japanese'][0]['reading']
				p = word + ' ' + '(' + reading + ')'
			except:
				word = diction['data'][0]['japanese'][0]['reading']
				p = word

		else:
			r = requests.post(url, data={'key': key, 'text': text, 'lang': lang})
			p = eval(r.text)['text'][0]	

		if check == 'true':
			await self.create_word(text,p)


		await self.send({
			'type':'websocket.send',
			'text': p
		})

	@database_sync_to_async
	def create_word(self, text, p):
		username = self.scope["user"]
		profile = Profile.objects.get(user=username)
		dic = profile.pack.task.all()
		obj , _ = Card.objects.get_or_create(rusw=text, engw=p, lang=profile.pack.lang)
		n=True
		for dic in dic:
			lin = dic.word
			if obj != lin:
				continue
			else: 
				n=False

		if n == True:
			task = Task.objects.create(word=obj)
			task.save()
			profile.pack.task.add(task)


	async def websocket_disconnect(self, event):
		print('disconnect')