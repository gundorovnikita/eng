from django.shortcuts import render, redirect
import requests
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from random import randint
import json
import re
from .models import *
import ast


url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?'
key = 'trnsl.1.1.20190221T162634Z.a43e789b72aeca90.fc65f57077cb3721380ba3e01d0366d2ffcfc0ee'

# Create your views here.
def index(request):
	return render(request, 'general/index.html')

def profile(request):
	profile = Profile.objects.get(user=request.user)
	word_all = profile.pack.task.all()
	count_all = word_all.count()
	process = profile.pack.task.filter(Q(choser=False) | Q(choser=None) | Q(chosen=False) | Q(chosen=None) | Q(compil=False) | Q(compil=None) | Q(inputr=False) | Q(inputr=None) | Q(inpute=False) | Q(inpute=None) | Q(yesno=False) | Q(yesno=None))
	count_process = process.count()
	count_learn = count_all - count_process
	select = request.GET.getlist('select')
	delete = request.GET.get('delete')
	if select :
		if delete == 'True':
			for select in select:
				task = profile.pack.task.filter(id=select).delete()
		else:
			for select in select:
				task = profile.pack.task.filter(id=select)
				for task in task:
					task.choser = None
					task.chosen = None
					task.compil = None
					task.inpute = None
					task.inputr = None
					task.save()
		return redirect('profile_url')
	context={
		'profile':profile,
		'count_all':count_all,
		'word_all':word_all,
		'process':process,
		'count_process':count_process,
		'count_learn':count_learn,
	}
	return render(request, 'general/profile.html', context)

def choice_lang(request):
	packs = Pack.objects.filter(user=request.user)
	profile = Profile.objects.get(user=request.user)
	answer = request.GET.get('chose')
	if answer:
		profile.pack, _  = Pack.objects.get_or_create(user=request.user, lang=answer)
		profile.save()
		return redirect('profile_url')
	context={
		'packs':packs,
		'profile':profile,
	}
	return render(request, 'general/choice_lang.html',context)

def lesson(request):
	profile = Profile.objects.get(user=request.user)
	choser = profile.pack.task.filter(Q(choser=False) | Q(choser=None)).count()
	chosen = profile.pack.task.filter(Q(chosen=False) | Q(chosen=None)).count()
	compil = profile.pack.task.filter(Q(compil=False) | Q(compil=None)).count()
	inpute = profile.pack.task.filter(Q(inpute=False) | Q(inpute=None)).count()
	inputr = profile.pack.task.filter(Q(inputr=False) | Q(inputr=None)).count()
	yesno = profile.pack.task.filter(Q(yesno=False) | Q(yesno=None)).count()
	context={
		'chosen':chosen,
		'choser':choser,
		'compil':compil,
		'inpute':inpute,
		'inputr':inputr,
		'yesno':yesno,
	}
	return render(request, 'general/lesson.html', context)

def add(request):
	return render(request, 'general/add.html')


#Игры:


def r_e(request):	
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(inpute=False).count() > 0:
		task = profile.pack.task.filter(inpute=False)[:4]
	else:
		if profile.pack.task.filter(inpute=None).count() > 0:
			reborn = profile.pack.task.filter(inpute=None)
			reborn.update(inpute=False)
			task = profile.pack.task.filter(inpute=False)[:4]
		else:
			return redirect('lesson_url')

	


	context={
		'task':task,
	}
	return render(request,'general/wi/r_e.html', context)


def e_r(request):
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(inputr=False).count() > 0:
		task = profile.pack.task.filter(inputr=False)[:4]
	else:
		if profile.pack.task.filter(inputr=None).count() > 0:
			reborn = profile.pack.task.filter(inputr=None)
			reborn.update(inputr=False)
			task = profile.pack.task.filter(inputr=False)[:4]
		else:
			return redirect('lesson_url')
	answer = request.GET.getlist('answer')
	n=0
	if answer:
		for answer in answer:
			now_task = task[n]
			print(now_task)
			print(n)		
			n+=1
			if answer==now_task.word.rusw:
				now_task.inputr = True
				now_task.save()
				n=n-1			
			else:
				now_task.inputr = None
				now_task.save()
				n=n-1

			
	context={
		'task':task,
	}
	return render(request,'general/wi/e_r.html', context)


def chr_e(request):
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(chosen=False).count() > 0:
		task = profile.pack.task.filter(chosen=False)[0]
	else:
		if profile.pack.task.filter(chosen=None).count() > 0:
			reborn = profile.pack.task.filter(chosen=None)
			reborn.update(chosen=False)
			task = profile.pack.task.filter(chosen=False)[0]
		else:
			return redirect('lesson_url')
	word = Card.objects.filter(lang=profile.pack.lang).order_by('?')[:3]
	answer = request.GET.get('chose')

	if answer:
		if answer=='true':
			q = 'Верно ', task.word.rusw, ' = ' ,task.word.engw
			task.chosen = True
			task.save()
		if answer=='chose':
			q = 'Не верно, ответ: ', task.word.rusw,  ' = ', task.word.engw
			task.chosen = None
			task.save()
	else:
		q='Введите перевод'

	context={
		'task':task,
		'word':word,
		'q':q,
	}
	return render(request,'general/wi/chr_e.html',context)

def che_r(request):
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(choser=False).count() > 0:
		task = profile.pack.task.filter(choser=False)[0]
	else:
		if profile.pack.task.filter(choser=None).count() > 0:
			reborn = profile.pack.task.filter(choser=None)
			reborn.update(choser=False)
			task = profile.pack.task.filter(choser=False)[0]
		else:
			return redirect('lesson_url')
	word = Card.objects.filter(lang=profile.pack.lang).order_by('?')[:3]
	answer = request.GET.get('chose')

	if answer:
		if answer=='true':
			q = 'Верно ', task.word.rusw, ' = ' ,task.word.engw
			task.choser = True
			task.save()
		if answer=='chose':
			q = 'Не верно, ответ: ', task.word.rusw,  ' = ', task.word.engw
			task.choser = None
			task.save()
	else:
		q='Введите перевод'

	context={
		'task':task,
		'word':word,
		'q':q,
	}
	return render(request,'general/wi/che_r.html',context)

def compil(request):
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(compil=False).count() > 0:
		task = profile.pack.task.filter(compil=False)[0]
	else:
		if profile.pack.task.filter(compil=None).count() > 0:
			reborn = profile.pack.task.filter(compil=None)
			reborn.update(compil=False)
			task = profile.pack.task.filter(compil=False)[0]
		else:
			return redirect('lesson_url')
	shuffled = ''.join(random.sample(task.word.engw, len(task.word.engw)))

	answer = request.GET.get('answer')

	if answer:
		if answer==task.word.engw:
			q = 'Верно'
			task.compil = True
			task.save()
		if answer!=task.word.engw:
			q = 'Не верно, ответ: ', task.word.engw
			task.compil = None
			task.save()
	else:
		q='Введите перевод'


	context={
		'task':task,
		'q':q,
		'shuffled':shuffled,
	}
	return render(request,'general/wi/compil.html', context)

def yesno(request):
	profile = Profile.objects.get(user=request.user)
	if profile.pack.task.filter(yesno=False).count() > 0:
		task = profile.pack.task.filter(yesno=False).order_by('?')[:7]
	else:
		if profile.pack.task.filter(yesno=None).count() > 0:
			reborn = profile.pack.task.filter(yesno=None)
			reborn.update(yesno=False)
			task = profile.pack.task.filter(yesno=False).order_by('?')[:7]
		else:
			return redirect('lesson_url')
	card = Card.objects.filter(lang=profile.pack.lang).order_by('?')[:3]

	context={
		'task':task,
		'card':card,
	}
	return render(request,'general/wi/yesno.html',context)




# AJAX 




def r_e_requestAjax(request):
	messageTrue = request.POST.get('messageTrue')
	testarray = ast.literal_eval(messageTrue)
	for testarray in testarray:
		task = Task.objects.get(id=testarray)
		task.inpute=True
		task.save()
	data = {
			'is_valid': False,}
	return JsonResponse(data)
