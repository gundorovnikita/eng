from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.urls import reverse

# Create your models here.
class Card(models.Model):
	engw = models.CharField(max_length=50)
	rusw = models.CharField(max_length=50)
	lang = models.CharField(max_length=7)
	def __str__(self):
		return str(self.rusw)


class Task(models.Model):
	word = models.ForeignKey(Card, on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	choser = models.NullBooleanField(default=False)
	chosen = models.NullBooleanField(default=False)
	compil = models.NullBooleanField(default=False)
	inputr = models.NullBooleanField(default=False)
	inpute = models.NullBooleanField(default=False)
	yesno = models.NullBooleanField(default=False)
	def __str__(self):
		return self.word.rusw + ' ' + str(self.date)


class Pack(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	task = models.ManyToManyField(Task, blank=True)
	point = models.PositiveIntegerField(default = 0)
	lang = models.CharField(max_length=7, default='ru-en')
	def __str__(self):
		return self.lang




class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) 
    point = models.PositiveIntegerField(default = 0)
    pack = models.ForeignKey(Pack, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.user)
