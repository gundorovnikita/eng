from django.urls import path, include
from .views import *

urlpatterns = [
    path('', index, name='index_url'),
    path('acc', profile, name='profile_url'),
    path('add', add, name='add_url'),
    path('lesson', lesson, name='lesson_url'),
    path('lesson/r_e', r_e, name='russia_url'),
    path('r_e_ajax_request/', r_e_requestAjax, name='r_e_ajax_request'),
    path('lesson/e_r', e_r, name='english_url'),
    path('lesson/chr_e', chr_e, name='chr_e_url'),
    path('lesson/che_r', che_r, name='che_r_url'),
    path('lesson/compil', compil, name='compil_url'),
    path('lesson/yesno', yesno, name='yesno_url'),
    path('acc/lang', choice_lang, name='choice_lang_url'),
]
