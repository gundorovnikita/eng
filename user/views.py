from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from general.models import Profile, Pack
# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('index_url'))
                else:
                    return HttpResponseRedirect('User is not active')
            else:
                return HttpResponse('User is None')
    else:
        form = UserLoginForm()
    context={
        'form':form,
    }
    return render(request, 'register/login.html', context)

def user_logout(request):
    logout(request)
    return redirect('user_login_url')

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()         
            np = Pack.objects.create(user=new_user)
            Profile.objects.create(user=new_user, pack = np)
            login(request, new_user)
            return redirect('index_url')
    else:
        form = UserRegistrationForm()
    context = {
        'form':form,
    }
    return render(request, 'register/register.html', context)